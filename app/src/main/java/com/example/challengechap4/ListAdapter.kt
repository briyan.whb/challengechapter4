package com.example.challengechap4

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.challengechap4.databinding.ListDataBinding
import com.example.challengechap4.dialog.EditDialog
import com.example.challengechap4.dialog.HapusData
import com.example.challengechap4.dialog.LihatData
import com.example.challengechap4.room.Catatan

class ListAdapter() : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var catatanList = emptyList<Catatan>()
    private var _binding : ListDataBinding? = null
    private val binding get() = _binding!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        _binding = ListDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return catatanList.size
    }

    inner class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val judul : TextView = binding.tvJudul
        val delete : ImageButton = binding.btnDelete
        val edit : ImageButton = binding.btnEdit
        val lihat = binding.lihatData
        val activity = itemView.context as? MainActivity
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = catatanList[position]
        val shared : SharedPreferences = holder.activity!!.getSharedPreferences("onBoard", Context.MODE_PRIVATE)

        holder.judul.text = currentItem.judul
        holder.delete.setOnClickListener {
            val delete = HapusData(currentItem)
            delete.show(holder.activity.supportFragmentManager, "delete")
        }
        holder.edit.setOnClickListener {
            val editor : SharedPreferences.Editor = shared.edit()
            editor.apply{
                putInt("ID_KEY", currentItem.id)
                putString("JUDUL_KEY", currentItem.judul)
                putString("CATATAN_KEY", currentItem.catatan)
                apply()
            }
            val edit = EditDialog()
            edit.show(holder.activity.supportFragmentManager, "edit")
        }
        holder.lihat.setOnClickListener {
            val editor : SharedPreferences.Editor = shared.edit()
            editor.apply{
                putInt("ID_KEY", currentItem.id)
                putString("JUDUL_KEY", currentItem.judul)
                putString("CATATAN_KEY", currentItem.catatan)
                apply()
            }
            val lihat = LihatData()
            lihat.show(holder.activity.supportFragmentManager, "lihat")
        }
    }

    fun setData(catatan: List<Catatan>){
        this.catatanList = catatan
        notifyDataSetChanged()
    }
}
