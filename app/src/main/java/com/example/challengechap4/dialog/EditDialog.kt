package com.example.challengechap4.dialog

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengechap4.MainActivity
import com.example.challengechap4.databinding.EditDataBinding
import com.example.challengechap4.room.Catatan
import com.example.challengechap4.room.CatatanViewModel
import com.example.challengechap4.room.DatabaseCatatan
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class EditDialog : DialogFragment() {

    private lateinit var mCatatanViewModel: CatatanViewModel
    private var _binding : EditDataBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = EditDataBinding.inflate(layoutInflater)

        mCatatanViewModel = ViewModelProvider(requireActivity())[CatatanViewModel::class.java]
        val shared : SharedPreferences = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val judulID : String? = shared.getString("JUDUL_KEY", "")
        val catatanID : String? = shared.getString("CATATAN_KEY", "")

        binding.edjudul.setText(judulID)
        binding.edCatatan.setText(catatanID)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnEdit.setOnClickListener {
            updateItem()
        }

        binding.btnCancel.setOnClickListener {
            Toast.makeText(requireContext(), "Cancel", Toast.LENGTH_LONG).show()
            dismiss()
        }
    }

    private fun updateItem() {
        val judul = binding.edjudul.text.toString()
        val catatan = binding.edCatatan.text.toString()
        val shared: SharedPreferences =
            requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val id = shared.getInt("ID_KEY", 0)
        val updated = Catatan(id, judul, catatan)
        val ed = DatabaseCatatan.getDatabase(requireContext())

        GlobalScope.async {
            val edit = ed.catatanDao().updateCatatan(updated)

            (requireContext() as MainActivity).runOnUiThread {
                if (edit != 0) {
                    Toast.makeText(
                        requireContext(),
                        "Sukses mengubah $judul",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Gagal mengubah $judul",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
        dismiss()
    }
}
