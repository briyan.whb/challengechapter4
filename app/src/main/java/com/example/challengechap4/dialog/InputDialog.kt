package com.example.challengechap4.dialog

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengechap4.databinding.InputDataBinding
import com.example.challengechap4.room.Catatan
import com.example.challengechap4.room.CatatanViewModel

class InputDialog : DialogFragment() {

    private lateinit var mCatatanViewModel : CatatanViewModel
    private var _binding : InputDataBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = InputDataBinding.inflate(layoutInflater)

        mCatatanViewModel = ViewModelProvider(requireActivity())[CatatanViewModel::class.java]

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnInput.setOnClickListener {
            insert()
        }
        binding.btnCancel.setOnClickListener {
            Toast.makeText(requireContext(), "Cancel", Toast.LENGTH_LONG).show()
            dismiss()
        }
    }

    private fun insert() {
        val judul = binding.edjudul.text.toString()
        val catatan = binding.edCatatan.text.toString()

        if (inputCheck(judul, catatan)){
            val input = Catatan(0, judul, catatan)
            mCatatanViewModel.addCatatan(input)
            Toast.makeText(requireContext(), "Berhasil!", Toast.LENGTH_LONG).show()
            dismiss()
        } else {
            Toast.makeText(requireContext(), "Data Kosong.", Toast.LENGTH_LONG).show()
            dismiss()
        }
    }

    private fun inputCheck(judul: String, catatan: String): Boolean{
        return !(TextUtils.isEmpty(judul) && TextUtils.isEmpty(catatan))
    }
}
