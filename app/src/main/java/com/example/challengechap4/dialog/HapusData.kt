package com.example.challengechap4.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengechap4.MainActivity
import com.example.challengechap4.databinding.HapusDataBinding
import com.example.challengechap4.room.Catatan
import com.example.challengechap4.room.CatatanViewModel
import com.example.challengechap4.room.DatabaseCatatan
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class HapusData(catatan: Catatan) : DialogFragment() {

    private lateinit var mCatatanViewModel : CatatanViewModel
    private var _binding : HapusDataBinding? = null
    private val binding get() = _binding!!
    val id = catatan

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HapusDataBinding.inflate(layoutInflater)

        mCatatanViewModel = ViewModelProvider(requireActivity())[CatatanViewModel::class.java]

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnHapus.setOnClickListener {
            val del = DatabaseCatatan.getDatabase(requireContext())

            GlobalScope.async {
                val delete = del.catatanDao().deleteCatatan(id)

                (requireContext() as MainActivity).runOnUiThread {
                    if (delete != 0) {
                        Toast.makeText(requireContext(),
                            "Data dihapus", Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(requireContext(),
                            "Data gagal dihapus", Toast.LENGTH_LONG)
                            .show()
                    }

                }
            }
            dismiss()
        }

        binding.btnCancel.setOnClickListener {
            Toast.makeText(requireContext(), "Cancel", Toast.LENGTH_LONG).show()
            dismiss()
        }
    }
}