package com.example.challengechap4.dialog

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengechap4.databinding.LihatDataBinding
import com.example.challengechap4.room.CatatanViewModel

class LihatData : DialogFragment() {

    private lateinit var mCatatanViewModel : CatatanViewModel
    private var _binding : LihatDataBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LihatDataBinding.inflate(layoutInflater)

        mCatatanViewModel = ViewModelProvider(requireActivity())[CatatanViewModel::class.java]

        val shared : SharedPreferences = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val judul : String = shared.getString("JUDUL_KEY", "").toString()
        val catatan : String = shared.getString("CATATAN_KEY", "").toString()
        binding.tvjudul.text = judul
        binding.tvCatatan.text = catatan

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}