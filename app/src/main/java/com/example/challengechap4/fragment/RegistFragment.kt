package com.example.challengechap4.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.challengechap4.R
import com.example.challengechap4.databinding.FragmentRegistBinding

class RegistFragment : Fragment() {

    private var _binding : FragmentRegistBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegistBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.confirm.setOnClickListener{
            val user : String = binding.edUser.text.toString()
            val email : String = binding.edEmail.text.toString()
            val pass : String = binding.edPass.text.toString()
            val passX : String  = binding.edPassX.text.toString()
            val shared : SharedPreferences = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)

            shared.getString("USER_KEY", "")
            shared.getString("EMAIL_KEY", "")
            shared.getString("PASS_KEY", "")

            when {
                user.isEmpty() or email.isEmpty() or pass.isEmpty() -> {
                    Toast.makeText(requireContext(), "Data Kosong", Toast.LENGTH_LONG).show()
                }
                pass != passX -> {
                    Toast.makeText(requireContext(), "Password Berbeda", Toast.LENGTH_LONG).show()
                }
                else -> {
                    val edit : SharedPreferences.Editor = shared.edit()
                    edit.apply{
                        putString("USER_KEY", user)
                        putString("EMAIL_KEY", email)
                        putString("PASS_KEY", pass)
                        apply()
                    }
                    Toast.makeText(requireContext(), "Registrasi Berhasil", Toast.LENGTH_LONG).show()
                    Navigation.findNavController(view).navigateUp()
                }
            }
            }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}