package com.example.challengechap4.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechap4.ListAdapter
import com.example.challengechap4.R
import com.example.challengechap4.databinding.FragmentHomeBinding
import com.example.challengechap4.dialog.InputDialog
import com.example.challengechap4.room.CatatanViewModel

class HomeFragment : Fragment() {

    private lateinit var mCatatanViewModel: CatatanViewModel
    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater)

        val adapter = ListAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        mCatatanViewModel = ViewModelProvider(this)[CatatanViewModel::class.java]
        mCatatanViewModel.readAllData.observe(viewLifecycleOwner) { catatan ->
            adapter.setData(catatan)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val shared : SharedPreferences = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val user = shared.getString("USER_KEY", "")
        binding.textView2.text = "Welcome $user"

        binding.add.setOnClickListener{
            val input = InputDialog()
            input.show(requireActivity().supportFragmentManager, "inputDialog")
        }

        binding.logout.setOnClickListener {
            val dialog = AlertDialog.Builder(requireActivity())
            dialog.setTitle("Yakin?")
            dialog.setCancelable(false)
            dialog.setIcon(R.mipmap.ic_launcher)
            dialog.setPositiveButton("Iya"){dialogInterface, i ->
                onBoarding()
                Toast.makeText(requireContext(), "Anda Telah Logout", Toast.LENGTH_LONG).show()
                Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_loginFragment)
            }
            dialog.setNegativeButton("Tidak"){dialogInterface, i->
            }
            dialog.show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun onBoarding(){
        val sharedPref = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean("Finished", false)
        editor.apply()
    }
}
