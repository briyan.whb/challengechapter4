package com.example.challengechap4.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.challengechap4.R
import com.example.challengechap4.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val shared : SharedPreferences = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)

        binding.login.setOnClickListener{
            val email : String = binding.edEmail.text.toString()
            val pass : String = binding.edPass.text.toString()

            when {
                email.isEmpty() && pass.isEmpty() -> {
                    Toast.makeText(requireContext(), "Email dan Password Kosong", Toast.LENGTH_LONG).show()
                }
                email.isEmpty() -> {
                    Toast.makeText(requireContext(), "Email Kosong", Toast.LENGTH_LONG).show()
                }
                pass.isEmpty() -> {
                    Toast.makeText(requireContext(), "Password Kosong", Toast.LENGTH_LONG).show()
                }
                else -> {
                    val emailID = shared.getString("EMAIL_KEY", "")
                    val passID = shared.getString("PASS_KEY", "")
                    if (emailID.equals(email) && passID.equals(pass)) {
                        onBoarding()
                        Toast.makeText(requireContext(), "Berhasil Login", Toast.LENGTH_LONG).show()
                        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
                    }else{
                        Toast.makeText(requireContext(), "User Belum Registrasi", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        binding.register.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun onBoarding(){
        val sharedBoard = requireActivity().getSharedPreferences("onBoard", Context.MODE_PRIVATE)
        val editor = sharedBoard.edit()
        editor.putBoolean("Finished", true)
        editor.apply()
    }
}