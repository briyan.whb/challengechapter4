package com.example.challengechap4.room

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CatatanViewModel(application: Application) : AndroidViewModel(application) {
    val readAllData: LiveData<List<Catatan>>
    private val repository: CatatanRepository

    init {
        val catatanDao = DatabaseCatatan.getDatabase(
            application
        ).catatanDao()
        repository = CatatanRepository(catatanDao)
        readAllData = repository.readAllData
    }

    fun addCatatan(catatan: Catatan){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addCatatan(catatan)
        }
    }

    fun updateCatatan(catatan: Catatan){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateCatatan(catatan)
        }
    }

    fun deleteCatatan(catatan: Catatan){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteCatatan(catatan)
        }
    }
}