package com.example.challengechap4.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Catatan::class], version = 1)
abstract class DatabaseCatatan : RoomDatabase() {
    abstract fun catatanDao() : CatatanDao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseCatatan? = null

        fun getDatabase(context: Context): DatabaseCatatan{
            val temp = INSTANCE
            if (temp != null) {
                return temp
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DatabaseCatatan::class.java,
                    "catatan_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}