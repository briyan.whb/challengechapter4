package com.example.challengechap4.room

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "catatan_table")
data class Catatan(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var judul: String,
    var catatan: String
): Parcelable
