package com.example.challengechap4.room

import androidx.lifecycle.LiveData

class CatatanRepository(private val catatanDao: CatatanDao) {
    val  readAllData : LiveData<List<Catatan>> = catatanDao.readAllData()

    suspend fun addCatatan ( catatan: Catatan ){
        catatanDao.addCatatan(catatan)
    }

    suspend fun updateCatatan ( catatan: Catatan ){
        catatanDao.addCatatan(catatan)
    }

    suspend fun deleteCatatan (catatan: Catatan){
        catatanDao.addCatatan(catatan)
    }
}