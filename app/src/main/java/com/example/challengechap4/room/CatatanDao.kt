package com.example.challengechap4.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.challengechap4.room.Catatan

@Dao
interface CatatanDao {
    @Query("SELECT * FROM catatan_table ORDER BY id ASC")
    fun readAllData() : LiveData<List<Catatan>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addCatatan(catatan: Catatan)

    @Update
    suspend fun updateCatatan(catatan: Catatan) : Int

    @Delete
    suspend fun deleteCatatan(catatan: Catatan) : Int
}